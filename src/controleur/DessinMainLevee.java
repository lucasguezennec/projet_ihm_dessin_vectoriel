package controleur;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.SwingUtilities;

import model.DessinModele;
import model.FigureColoree;
import model.Point;

/**
 * Creation de la classe pour le dessin a main leve
 */
public class DessinMainLevee implements MouseListener, MouseMotionListener {
	/**
	 * attribut DessinModele dm, modele du dessin voulu
	 */
	private DessinModele dm;
	
	/**
	 * Constructeur de la classe qui initialise le dessin 
	 * @param dessin, dessin vide 
	 */
	public DessinMainLevee(DessinModele dessin) {
		this.dm = dessin;
	}
	/**
	 * methode qui regarde ce que l'on fait quand on reste appuye 
	 * avec la souris et qu'on la deplace 
	 */
	@Override
	public void mouseDragged(MouseEvent e) {
		if(SwingUtilities.isLeftMouseButton(e)) {
			this.dm.miseAJourSelection();
			this.dm.getListeMainLevee().remove(this.dm.getFigureMainLeveeEnCours());
			int x = e.getX();
			int y = e.getY();
			Point p = new Point(x,y);
			this.dm.getFigureMainLeveeEnCours().getFigureMainLeveeEnCours().add(p);
			this.dm.getListeMainLevee().add(this.dm.getFigureMainLeveeEnCours());
			
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	/**
	 * methode qui permet de voir ce que l'on fait quand 
	 * on appuie sur un bouton de la souris
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		if(SwingUtilities.isLeftMouseButton(e)){
			int x = e.getX();
			int y = e.getY();
			this.dm.getFigureMainLeveeEnCours().getFigureMainLeveeEnCours().add(new Point(x,y));
			this.dm.miseAJourSelection();
		}
		if(SwingUtilities.isMiddleMouseButton(e)){
			this.dm.getListeMainLevee().clear();
			this.dm.miseAJourSelection();
		}

		
	}
	/**
	 * methode permettant de voir ce que l'on fait quand on relache 
	 * un bouton de la souris 
	 */
	@Override
	public void mouseReleased(MouseEvent e) {
		this.dm.finFigureMainlevee();
		this.dm.miseAJourSelection();
	}

}
