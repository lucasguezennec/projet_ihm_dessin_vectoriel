package controleur;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import model.DessinModele;
import model.FigureColoree;
import model.Point;
/**
 * Creation de la classe FabricantFigure
 */
public class FabricantFigures implements MouseListener{
	/**
	 * attribut DessinModele qui contiendra le dessin voulu
	 */
	private DessinModele model;
	
	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	/**
	 * methode qui permet d'ajouter un point quand on clique
	 * sur un bouton de la souris
	 */
	@Override
	public void mousePressed(MouseEvent m) {
		this.model.ajoutePoint(m.getX(), m.getY());
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	/**
	 * Constructeur de FabricntFigure qui initialise le dessin avec un dessin vide
	 * @param dm
	 */
	public FabricantFigures(DessinModele dm) {
		super();
		this.model = dm;
	}
	
}
