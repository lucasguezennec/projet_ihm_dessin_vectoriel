package controleur;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.SwingUtilities;

import model.DessinModele;
import model.FigureColoree;
import model.Point;
import model.Polygone;
import model.Quadrilatere;
/**
 * Creation de la classe ManipulateurFormes pour manipuler les formes
 */
public class ManipulateurFormes implements MouseListener, MouseMotionListener {
	/**
	 * attribut DessinModele qui contiendra le dessin voulu
	 */
	private DessinModele model;
	/**
	 * attribut FigureColoree qui contiendra la figure en cours
	 * de manipulation
	 */
	private FigureColoree figureEnCourDeManipulation;
	/**
	 * attributs entier qui contiendront les anciennes coordonnees de la souris
	 * a un moment donne
	 */
	private int ancienXSouris, ancienYSouris;

	@Override
	public void mouseClicked(MouseEvent arg0) {
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}
	/**
	 * methode qui permet de voir ce que l'on fait quand on appuie sur
	 * un bouton de la souris
	 */
	@Override
	public void mousePressed(MouseEvent arg0) {
		if(SwingUtilities.isLeftMouseButton(arg0)) {
			int x = (int)arg0.getX();
			int y = (int)arg0.getY();
			curseurDansFigure(x,y);
			ancienXSouris =(int) arg0.getX();
			ancienYSouris =(int) arg0.getY();

		}

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {

	}
	/**
	 * Constructeur ManipulateurFormes qui initialise tous les attributs
	 * @param dm, dessin vide
	 */
	public ManipulateurFormes(DessinModele dm) {
		super();
		this.model = dm;
		this.ancienXSouris = 0;
		this.ancienYSouris = 0;
		this.figureEnCourDeManipulation = new Quadrilatere();
	}
	/**
	 * methode qui permet de deselectionner toutes les figures deja creees
	 */
	public void toutDeselectionne() {
		for(int i = 0; i < this.model.getListeFigure().size(); i++){
			this.model.getListeFigure().get(i).deSelectionne();
			this.model.miseAJourSelection();
		}
	}
	/**
	 * methode permettant de savoir si le curseur est dans la figure ou pas 
	 * @param x, coordonnee x de la souris
	 * @param y, coordonnee y de la souris
	 * @return un booleen pour savoir si le curseur est dans la figure ou non
	 */
	public boolean curseurDansFigure(double x, double y) {
		int iterateur = this.model.getListeFigure().size()-1;
		boolean trouve = false;
		toutDeselectionne();
		while(!trouve && iterateur >= 0) {
			if(this.model.getListeFigure().get(iterateur).estDedans(x,y)) {
				trouve=true;
				this.model.getListeFigure().get(iterateur).selectionne();
				this.model.setFigureEnCoursDeManipulation(this.model.getListeFigure().get(iterateur));
				this.figureEnCourDeManipulation = this.model.getListeFigure().get(iterateur);
				this.model.miseAJourSelection();
			}
			iterateur--;
		}
		return trouve;
	}
	/**
	 * methode permettant de faire bouger les figures selectionnees
	 * quand on reste appuye sur le bouton gauche de la souris
	 */
	@Override
	public void mouseDragged(MouseEvent arg0) {
		if(SwingUtilities.isLeftMouseButton(arg0)) {
			int x = (int)arg0.getX();
			int y = (int)arg0.getY();
			rechercheFigureSelect();
			Point[] tP = this.figureEnCourDeManipulation.getTab_mem();
			int differenceCoordX = 0;
			int differenceCoordY = 0;
			differenceCoordX = (int)(x-this.ancienXSouris);
			differenceCoordY = (int)(y-this.ancienYSouris);
			
			if(curseurDansFigure(x,y)) {
				
				for(int i= 0; i < tP.length;i++) {
					tP[i].incrementerX(differenceCoordX);
					tP[i].incrementerY(differenceCoordY);
				}
				this.figureEnCourDeManipulation.translater(tP);
				this.model.miseAJourSelection();
			}
			this.ancienXSouris = x;
			this.ancienYSouris = y;
		}


	}
	/**
	 * methode qui va regarder dans la liste de figure en cours laquelle est 
	 * selectionnee
	 */
	public void rechercheFigureSelect() {
		boolean trouve =false;
		int i =0;
		while(!trouve && i < this.model.getListeFigure().size()) {

			if(this.model.getListeFigure().get(i).isSelected()) {
				trouve=true;
				this.figureEnCourDeManipulation = this.model.getListeFigure().get(i);
			}
			i++;
		}
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
	}

}
