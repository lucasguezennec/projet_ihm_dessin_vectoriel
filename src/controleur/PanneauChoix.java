 package controleur;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import model.Carre;
import model.DessinModele;
import model.FigureColoree;
import model.Losange;
import model.Quadrilatere;
import model.Rectangle;
import model.Triangle;
import vue.VueDessin;
/**
 * Creation de la classe PanneauChoix
 */
public class PanneauChoix extends JPanel {
	/**
	 * attribut FigureColoree qui contiendra la figure que l'on souhaite
	 */
	private FigureColoree fc;
	/**
	 * attribut DessinModele qui contiendra le dessin voulu
	 */
	private DessinModele dessin;
	/**
	 * attribut VueDessin qui contiendra l'espace ou l'on dessinera
	 */
	private VueDessin vdessin;
	/**
	 * Constructeur de PanneauChoix qui initialise tout les attributs et 
	 * qui cree tous les boutons necessaires
	 * @param dm, dessin vide
	 * @param vueDessin, nouveau JPanel ou l'on dessinera
	 */
	public PanneauChoix(DessinModele dm, VueDessin vueDessin) {
		this.dessin = dm;
		this.vdessin = vueDessin;
		this.setPreferredSize(new Dimension(800,100));
		this.setLayout(new BorderLayout());
		
		//Mise en place des JPanels
		JPanel bouttonChoix = new JPanel();
		JPanel bouttonCombo = new JPanel();
		fc = new Quadrilatere();
		
		//Creation des elements interactions
		ButtonGroup bg = new ButtonGroup();

		JRadioButton nouvelleFig = new JRadioButton("Nouvelle figure");
		JRadioButton mainLeve = new JRadioButton("Trac� � main lev�e");
		JRadioButton manipulation = new JRadioButton("Manipulation");
		mainLeve.setSelected(true);
		final JComboBox c = new JComboBox(new String[] {"Quadrilatere","Rectangle","Triangle","Carre","Losange"});
		final JComboBox choixCouleur = new JComboBox(new String[] {"Noir", "Rouge", "Vert", "Orange","Bleu","Cyan","Gris","Magenta","Rose","Jaune"});
		c.setEnabled(false);

		//Ajout sur le JPanel
		bg.add(nouvelleFig);
		bg.add(mainLeve);
		bg.add(manipulation);


		bouttonChoix.add(nouvelleFig);
		bouttonChoix.add(mainLeve);
		bouttonChoix.add(manipulation);
		bouttonCombo.add(c);
		bouttonCombo.add(choixCouleur);
		this.add(bouttonChoix,BorderLayout.NORTH);
		this.add(bouttonCombo,BorderLayout.CENTER);
		
		//Activer choix des figures
		nouvelleFig.addActionListener(new ActionListener() {
			/**
			 * methode interne qui permet de faire de nouvelle figure en enlevant 
			 * les listeners deja presents et en recreant d'autres
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				clearListener();
				c.setEnabled(true);
				dm.setManipulation(false);
				dm.setNbClic(0);
				FabricantFigures fg = new FabricantFigures(dessin);
				vdessin.addMouseListener(fg);
			}

		});
		
		
		//Desactiver choix des figures
		ActionListener bouttonDesacBox = new ActionListener() {
			/**
			 * methode qui permet de desactiver le choix du bouton Nouvelle Figure
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				c.setEnabled(false);

			}

		};
		mainLeve.addActionListener(bouttonDesacBox);
		manipulation.addActionListener(bouttonDesacBox);


		//Choix des figures
		c.addActionListener(new ActionListener() {
			/**
			 * methode qui permet de savoir quelle figure est demandee
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				fc = creeFigure(c.getSelectedIndex());
				dm.setNbClic(0);
				dm.setFigureEnCours(fc);
				dm.notifyObservers();
			}

		});

		//Choix des couleurs
		choixCouleur.addActionListener(new ActionListener() {
			/**
			 * methode qui permet de changer de couleur pour une figure selectionnee
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				 dm.getFigureEnCours().changeCouleur(determineCouleur(choixCouleur.getSelectedIndex())); 
				 if(dm.isManipulation()) {
					 dm.getFigureEnCoursDeManipulation().changeCouleur(determineCouleur(choixCouleur.getSelectedIndex()));
					 dm.miseAJourSelection();
				 }
				 dm.getFigureMainLeveeEnCours().setC(determineCouleur(choixCouleur.getSelectedIndex()));

			}

		});
		
		//Bouton Manipulation
		manipulation.addActionListener(new ActionListener() {
			/**
			 * methode qui permet d'effacer tous les listeners pr�sents
			 * et d'en recreer d'autres dont il a besoin
			 */
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dm.setManipulation(true);
				clearListener();
				ManipulateurFormes mf = new ManipulateurFormes(dessin);
				vdessin.addMouseListener(mf);
				vdessin.addMouseMotionListener(mf);
			}
			
		});
		
		//Dessin � main lev�e
		mainLeve.addActionListener(new ActionListener() {
			/**
			 * methode qui enleve tous les listeners presents et qui 
			 * cree eux dont il a besoin
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				dm.setManipulation(false);
				clearListener();
				DessinMainLevee dl = new DessinMainLevee(dessin);
				vdessin.addMouseListener(dl);
				vdessin.addMouseMotionListener(dl);
			}
			
		});
		
		//Interface par defaut
		nouvelleFig.setSelected(true);
		c.setEnabled(true);
		dm.setManipulation(false);
		dm.setNbClic(0);
		FabricantFigures fg = new FabricantFigures(dessin);
		vdessin.addMouseListener(fg);
	}
	/**
	 * methode qui cree la figure selon l'index choisi dans le menu deroulant
	 * @param nb, index de la figure
	 * @return la figure a creer
	 */
	public FigureColoree creeFigure(int nb) {
		FigureColoree figC = null;
		switch(nb) {
		case 0:
			figC = new Quadrilatere();
			
			break;
		case 1:
			figC = new Rectangle();
			break;
		case 2:
			figC = new Triangle();
			break;
		case 3:
			figC = new Carre();
			break;
		case 4:
			figC = new Losange();
			break;
		}
		return figC;

	}
	/**
	 * methode qui permet d'enlever tous les listeners
	 */
	public void clearListener() {
		MouseListener[] mouseListeners = vdessin.getMouseListeners();
		for (MouseListener mouseListener : mouseListeners) {
		    vdessin.removeMouseListener(mouseListener);
		}
		MouseMotionListener[] mouseMotionListeners = vdessin.getMouseMotionListeners();
		for (MouseMotionListener mouseMotionListener : mouseMotionListeners) {
		    vdessin.removeMouseMotionListener(mouseMotionListener);
		}
	}
	/**
	 * methode qui permet de savoir la couleur que l'on veut par rapport � l'index
	 * choisi dans le menu deroulant
	 * @param col, index de la couleur
	 * @return la couleur choisi
	 */
	public Color determineCouleur(int col) {
		Color couleur =Color.BLACK;
		switch(col) {
		case 0:
			couleur =Color.BLACK;
			break;
		case 1:
			couleur =Color.RED;
			break;
		case 2:
			couleur =Color.GREEN;
			break;
		case 3:
			couleur =Color.ORANGE;
			break;
		case 4:
			couleur = Color.blue;
			break;
		case 5:
			couleur = Color.CYAN;
			break;
		case 6:
			couleur = Color.gray;
			break;
		case 7:
			couleur = Color.MAGENTA;
			break;
		case 8:
			couleur = Color.pink;
			break;
		case 9:
			couleur = Color.yellow;
			break;
			
		}
		return couleur;	
	}
	/**
	 * methode qui permet de faire l'affichage des composants graphiques
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
	}


}
