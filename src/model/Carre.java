package model;

import java.awt.Polygon;
/**
 * Creation de la classe Carre
 */
public class Carre extends Rectangle {
	/**
	 * Constructeur de Carre
	 */
	public Carre() {
		super();
	}
	/**
	 * methode qui permet d'avoir le nombre de clics necessaires
	 * pour creer un carre
	 */
	public int nbClics() {
		return 2;	
	}
	
	
	/**
	 * methode qui permet de modifer les points du carre quand on 
	 * doit le deplacer
	 */
	@Override
	public void modifierPoints(Point[] p) {
		this.setP(new Polygon());
		int longueur = p[1].rendreX() - p[0].rendreX();
		if(this.nbClics()==2) {
			Point[] res = {p[0],new Point(p[1].rendreX(),p[0].rendreY()),new Point(p[0].rendreX()+longueur,p[0].rendreY()+longueur),new Point(p[0].rendreX(), p[0].rendreY()+longueur)};
			for(int i = 0; i < res.length;i++) {
				this.tab_mem[i]=res[i];
				this.getP().addPoint(res[i].rendreX(), res[i].rendreY());
			}

		}
	}
	/**
	 * methode qui permet de copier la figure
	 */
	public FigureColoree copieDefigure() {
		Carre res = new Carre();
		res.changeCouleur(this.getCouleur());
		for(int i=0; i < this.nbPoints()-1; i++) {
			res.getTab_mem()[i] = this.getTab_mem()[i];
		}
		return res;
	}
}
