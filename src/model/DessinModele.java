package model;

import java.awt.Color;
import java.awt.Polygon;
import java.util.ArrayList;
import java.util.Observable;
/**
 * Creation de la classe DessinModele
 */
public class DessinModele extends Observable{
	/**
	 * attribut entier qui contiendra le nombre de clic de la figure
	 */
	private int nbClic;
	/**
	 * attribut tableau de Point qui contiendra le nombre de clics que l'on 
	 * a fait
	 */
	private Point[] points_cliques;
	/**
	 * attribut Liste de FigureColoree qui contiendra toutes les figures que 
	 * l'on a cree
	 */
	private ArrayList<FigureColoree> lfc;
	/**
	 * attribut Liste de FigureMainLevee qui contiendra les traces a main levee 
	 */
	private ArrayList<FigureMainLevee> listeMainLevee;
	/**
	 * attribut FigureMainLevee qui contiendra le trace en cours
	 */
	private FigureMainLevee figureMainLeveeEnCours;
	/**
	 * atrribut FigureColoree qui contiendra la figure en cours de creation
	 */
	private FigureColoree figureEnCours;
	/**
	 * attribut FigureColoree qui contiendra la figure en cours de manipulation
	 */
	private FigureColoree figureEnCoursDeManipulation;
	/**
	 * attribut booleen qui regardera si la figure est manipulee ou non
	 */
	private boolean manipulation;
	/**
	 * methode getter qui regarde la figure qui est en cours de manipulation
	 * @return la figure qui est en cours de manipulation
	 */
	public FigureColoree getFigureEnCoursDeManipulation() {
		return figureEnCoursDeManipulation;
	}

	/**
	 * methode setter qui initialise la figure en cours de manipulation avec celle
	 * passee en parametre
	 * @param figureEnCoursDeManipulation, figure coloree en cours de manipulation
	 */
	public void setFigureEnCoursDeManipulation(FigureColoree figureEnCoursDeManipulation) {
		this.figureEnCoursDeManipulation = figureEnCoursDeManipulation;
	}

	/**
	 * methode getter qui va chercher le trace en cours
	 * @return le trace qui est en cours
	 */
	public FigureMainLevee getFigureMainLeveeEnCours() {
		return figureMainLeveeEnCours;
	}
	/**
	 * methode qui va chercher la liste des traces 
	 * @return la liste des traces
	 */
	public ArrayList<FigureMainLevee> getListeMainLevee() {
		return listeMainLevee;
	}

	/**
	 * methode qui regarde l'etat du booleen manipulation
	 * @return vrai si la figure est manipulee, faux si elle ne l'est pas
	 */
	public boolean isManipulation() {
		return manipulation;
	}
	/**
	 * methode setter qui initialise le booleen avec celui passe en parametre
	 * @param manipulation, boolean qui dit si la figure est manipulee ou non
	 */
	public void setManipulation(boolean manipulation) {
		this.manipulation = manipulation;
	}
	/**
	 * methode getter qui va chercher le nombre de clics d'une figure
	 * @return le nombre de clics d'une figure
	 */
	public int getNbClic() {
		return nbClic;
	}
	/**
	 * methode setter qui initialise le nombre de clics avec celui
	 * passe en parametre
	 * @param nbClic, nombre de clics d'une figure
	 */
	public void setNbClic(int nbClic) {
		this.nbClic = nbClic;
	}
	/**
	 * Constructeur de DessinModele 
	 */
	public DessinModele() {
		this.nbClic=0;
		this.figureEnCours = new Quadrilatere();
		this.points_cliques = new Point[this.figureEnCours.nbPoints()];
		this.lfc = new ArrayList<FigureColoree>();
		this.figureMainLeveeEnCours = new FigureMainLevee();
		this.listeMainLevee = new ArrayList<FigureMainLevee>();
	}
	/**
	 * methode qui ajoute la figure passe en parametre dans la liste 
	 * de figure
	 * @param fc, figure a ajouter
	 */
	public void ajoute(FigureColoree fc) {
		this.lfc.add(fc);
	}
	/**
	 * methode qui permet de changer de couleur pour une figure
	 * @param fc, figure sur laquelle on veut changer la couleur
	 * @param c, couleur que l'on veut
	 */
	public void changeColor(FigureColoree fc, Color c) {
		int index = this.lfc.indexOf(fc);
		this.lfc.get(index).changeCouleur(c);
		setChanged();
		notifyObservers();
	}
	/**
	 * methode qui initialise la figure en cours, le nombre de clics
	 * et le tableau de points cliques
	 * @param fc, figure en cours
	 */
	public void construit(FigureColoree fc) {
		this.figureEnCours = fc;
		this.nbClic = 0;
		this.points_cliques = new Point[fc.nbPoints()];
	}
	/**
	 * methode permettant d'ajouter un point dans la FigureMainLeveeEnCours
	 * @param pX, coordonee x de la souris
	 * @param pY, coordonne y de la souris
	 */
	public void ajoutePointMainLevee(int pX, int pY) {
		this.figureMainLeveeEnCours.getFigureMainLeveeEnCours().add(new Point(pX,pY));
		this.listeMainLevee.remove(this.listeMainLevee.lastIndexOf(this.figureMainLeveeEnCours));
		this.listeMainLevee.add(this.figureMainLeveeEnCours);
		setChanged();
		notifyObservers();
	}
	/**
	 * methode qui perme d'ajouter le trace dans la liste des traces et 
	 * de reinitiliser les attributs necessaires
	 */
	public void finFigureMainlevee() {
		this.listeMainLevee.add(this.figureMainLeveeEnCours);
		Color c = this.figureMainLeveeEnCours.getC();
		this.figureMainLeveeEnCours = new FigureMainLevee();
		this.figureMainLeveeEnCours.setC(c);
	}

	/**
	 * methode permettant d'ajouter un point quand on clique
	 * @param pX, coordonne x de la souris
	 * @param pY, coordonne y de la souris
	 */
	public void ajoutePoint(int pX, int pY) {
		this.nbClic += 1;
		this.points_cliques[this.nbClic-1] = new Point(pX,pY);
		if (this.figureEnCours.nbClics() == this.nbClic) {
			this.lfc.add(this.figureEnCours);
			this.figureEnCours.modifierPoints(points_cliques);
			this.nbClic = 0;	
			ajoute(this.figureEnCours.copieDefigure());
			construit(this.lfc.get(this.lfc.size()-1));
		}
		setChanged();
		notifyObservers();
	}
	/**
	 * methode getter qui va chercher la liste de figure en cours
	 * @return la liste de figure en cours
	 */
	public ArrayList<FigureColoree> getListeFigure(){
		return this.lfc;
	}
	/**
	 * methode getter qui va chercher la figure en cours de creation
	 * @return la figure en cours de creation
	 */
	public FigureColoree getFigureEnCours() {
		return this.figureEnCours;

	}
	/**
	 * methode setter qui initialise la figure en cours avec celle passe 
	 * en parametre
	 * @param fc, figure en cours 
	 */
	public void setFigureEnCours(FigureColoree fc) {
		this.figureEnCours = fc;
	}
	/**
	 * methode setter qui initialise la liste de figure en cours avec celle
	 * passee en parmatre
	 * @param listefc, liste de figure en cours
	 */
	public void setListeFigure(ArrayList<FigureColoree> listefc) {
		this.lfc = listefc;
	}
	/**
	 * methode qui notifie les observateurs d'une mise a jour des composants
	 */
	public void miseAJourSelection() {
		setChanged();
		notifyObservers();
	}


}
