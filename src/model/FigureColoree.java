package model;

import java.awt.*;
/**
 * Creation de la classe abstraite FigureColoree
 */
public abstract class FigureColoree {
	/**
	 * constante entiere qui contient la taille des carres qui apparaitront 
	 * lors de la selection d'une figure
	 */
	public final static int TAILLE_CARRE_SELECTION = 10;
	/**
	 * attribut booleen qui regardera si la figure est selectionne ou non
	 */
	private boolean selected;
	/**
	 * attribut Color qui contiendra la couleur de la figure
	 */
	protected Color couleur;
	/**
	 * attribut tableau de Point qui contiendra les points de la figure
	 */
	protected Point[] tab_mem;
	/**
	 * methode setter qui initialise le tableau de points avec celui 
	 * passe en parametre
	 * @param tab_mem, tableau de points d'une figure
	 */
	public void setTab_mem(Point[] tab_mem) {
		this.tab_mem = tab_mem;
	}
	/**
	 * methode permettant de savoir si la figure est selectionnee ou non
	 * @return vrai si la figure est selectionnee, faux si elle ne l'est pas 
	 */
	public boolean isSelected() {
		return selected;
	}
	/**
	 * methode setter qui initialise le booleen avec celui passe en parametre
	 * @param selected, boolean pour savoir si la figure est selectionnee ou non
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	/**
	 * methode getter qui va chercher le tableau de points 
	 * @return le tableau de points 
	 */
	public Point[] getTab_mem() {
		return this.tab_mem;
	}
	/**
	 * Constructeur de FigureColoree
	 */
	public FigureColoree() {
		this.selected = false;
		this.tab_mem = new Point[nbPoints()];
	}
	/**
	 * methode qui affiche les carres de la selection quand la figure est selectionnee
	 * @param g
	 */
	public void affiche(Graphics g) {
		if(this.selected == true) {
			for (int i = 0; i < tab_mem.length; i++) {
				g.setColor(Color.BLACK);
				g.fillRect(this.tab_mem[i].rendreX()-TAILLE_CARRE_SELECTION/2, 
						this.tab_mem[i].rendreY()-TAILLE_CARRE_SELECTION/2,
						TAILLE_CARRE_SELECTION, TAILLE_CARRE_SELECTION);

			}
		}
	}
	/**
	 * methode qui met le booleen a vrai car il est selectionne 
	 */
	public void selectionne() {
		this.selected=true;
	}
	/**
	 * methode qui met le booleen a faux car la figure est deselectionnee
	 */
	public void deSelectionne() {
		this.selected=false;
	}
	/**
	 * methode permettant de changer la couleur de la figure avec celle passee 
	 * en parametre
	 * @param c, couleur de la figure
	 */
	public void changeCouleur(Color c) {
		this.couleur = c;
	}
	/**
	 * methode abstraite qui regarde si le curseur est dans la figure
	 * @param x, coordonnee x de la souris
	 * @param y, coordonnee y de la souris
	 * @return vrai si le curseur est dans la figure, faux si il n'y est pas
	 */
	public abstract boolean estDedans(double x, double y);
	/**
	 * methode abstraite qui regarde le nombre de points d'une figure
	 * @return le nombre de points de la figure
	 */
	public abstract int nbPoints();
	/**
	 * methode abstraite qui regarde le nombre de clics necessaires pour 
	 * construire la figure
	 * @return le nombre de clics necesaires pour faire la figure
	 */
	public abstract int nbClics();
	/**
	 * methode abstraite permettant de modifier les points de la figure quand 
	 * on la bouge
	 * @param p, le nouveau tableau de points 
	 */
	public abstract void modifierPoints(Point[] p);
	/**
	 * methode abstraite qui permet de copier la figure
	 * @return la figure copiee
	 */
	public abstract FigureColoree copieDefigure();
	/**
	 * methode abstraite permettant de deplacer la figure
	 * @param p, nouveau tableau de points 
	 */
	public abstract void translater(Point[] p);
	/**
	 * methode getter qui va chercher la couleur de la figure
	 * @return la couleur de la figure
	 */
	public Color getCouleur() {
		return couleur;
	}
	/**
	 * methode setter qui initialise la couleur de la figure avec celle 
	 * passee en parametre
	 * @param couleur, couleur de la figure 
	 */
	public void setCouleur(Color couleur) {
		this.couleur = couleur;
	}

}
