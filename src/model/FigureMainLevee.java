package model;

import java.awt.Color;
import java.util.ArrayList;
/**
 * Creation de la classe FigureMainLevee
 */
public class FigureMainLevee {
	/**
	 * attribut Liste de Point qui contiendra la liste de point pour le trace
	 */
	private ArrayList<Point> figureMainLeveeEnCours;
	/**
	 * attribut Color qui contiendra la couleur de trace
	 */
	private Color c;
	/**
	 * Constructeur De FigureMainLevee
	 */
	public FigureMainLevee() {
		this.figureMainLeveeEnCours = new ArrayList<Point>();
	}
	/**
	 * methode getter qui va chercher la couleur de trace
	 * @return la couleur de trace
	 */
	public Color getC() {
		return c;
	}
	/**
	 * methode setter qui initialise la couleur du trace avec celle passee
	 * en parametre
	 * @param c, couleur du trace
	 */
	public void setC(Color c) {
		this.c = c;
	}
	/**
	 * methode getter qui va chercher la liste de Points du trace
	 * @return la liste de Point du trace
	 */
	public ArrayList<Point> getFigureMainLeveeEnCours() {
		return figureMainLeveeEnCours;
	}
}
