package model;

import java.awt.Polygon;
/**
 * Creation de la classe Losange qui herite de Carre
 */
public class Losange extends Carre {
	/**
	 * Constructeur de Losange
	 */
	public Losange() {
		super();
	}
	/**
	 * methode permettant de modifier les points de la figure quand 
	 * on la bouge
	 * @param p, le nouveau tableau de points 
	 */
	@Override
	public void modifierPoints(Point[] p) {
		this.setP(new Polygon());
		int longueurY = p[1].rendreY() - p[0].rendreY();
		int longueurX = p[1].rendreX() - p[0].rendreX();
		if(this.nbClics()==2) {
			Point[] res = {p[0],p[1],new Point(p[0].rendreX(),p[0].rendreY()+(longueurY*2)),new Point(p[0].rendreX()-(longueurX), p[1].rendreY())};
			for(int i = 0; i < res.length;i++) {
				this.tab_mem[i]=res[i];
				this.getP().addPoint(res[i].rendreX(), res[i].rendreY());
			}

		}
	}
	/**
	 * methode qui permet de copier la figure
	 * @return la figure copiee
	 */
	public FigureColoree copieDefigure() {
		Losange res = new Losange();
		res.changeCouleur(this.getCouleur());
		for(int i=0; i < this.nbPoints()-1; i++) {
			res.getTab_mem()[i] = this.getTab_mem()[i];
		}
		return res;
	}
}
