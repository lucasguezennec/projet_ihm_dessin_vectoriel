package model;

import java.awt.Color;
/**
 * Creation de la classe Point
 */
public class Point {
	/**
	 * attributs entiers qui contiendront les coordonnees du point
	 */
	private int x, y;
	/**
	 * Constructeur de Point
	 * @param pX, coordonnee x du point
	 * @param pY, coordonnee y du point
	 */
	public Point(int pX, int pY) {
		this.x=pX;
		this.y=pY;
	}
	/**
	 * methode qui va chercher la valeur de x
	 * @return la valeur de x
	 */
	public int rendreX() {
		return x;
	}
	/**
	 * methode qui va chercher la valeur de y
	 * @return la valeur de y
	 */
	public int rendreY() {
		return y;
	}
	/**
	 * methode qui ajoute � x la valeur passee en parametre
	 * @param pX, valeur a ajouter
	 */
	public void incrementerX(int pX) {
		x += pX;
	}
	/**
	 * methode qui ajoute � y la valeur passee en parametre
	 * @param pY, valeur a ajouter
	 */
	public void incrementerY(int pY) {
		y += pY;
	}
	/**
	 * methode qui change la valeur de x avec celle passe en parametre
	 * @param pX, valeur a mettre dans x
	 */
	public void modifierX(int pX) {
		this.x=pX;
	}
	/**
	 * methode qui change la valeur de y avec celle passe en parametre
	 * @param pY, valeur a mettre dans y
	 */
	public void modifierY(int pY) {
		this.y=pY;
	}
	/**
	 * methode qui permet de changer les valeurs de x et y avec celles passees en parametre
	 * @param pX, valeur a mettre dans x
	 * @param pY, valeur a mettre dans y
	 */
	public void translation(int pX, int pY) {
		this.modifierX(pX);
		this.modifierY(pY);
	}
	
	/**
	 * methode qui regarde la distance qu'il y a entre deux points
	 * @param p2, deuxieme point
	 * @return la distance entre les deux points
	 */
	public double distance(Point p2) {
		double distance = 0;
		distance = Math.sqrt((Math.pow(this.x-p2.rendreX(), 2) + (Math.pow(this.y-p2.rendreY(), 2))));
		return distance;
		
	}
}
