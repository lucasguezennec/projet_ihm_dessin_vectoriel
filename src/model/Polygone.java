package model;

import java.awt.*;
/**
 * Creation de la classe abstraite Polygone qui herite de FigureColoree
 */
public abstract class Polygone extends FigureColoree{
	/**
	 * atribut Polygon qui contiendra le polygone que l'on veut creer
	 */
	private Polygon p;
	/**
	 * Constructeur de Polygone
	 */
	public Polygone() {
		super();
		this.p = new Polygon();
		
	}
	/**
	 * methode getter qui va chercher le polygone que l'on a
	 * @return le polygone que l'on a
	 */
	public Polygon getP() {
		return p;
	}
	/**
	 * methode setter qui initialise le polygone avec celui passe en parametre
	 * @param p, polygon a affecter
	 */
	public void setP(Polygon p) {
		this.p = p;
	}
	/**
	 * methode qui permet d'afficher le polygone que l'on a cree
	 */
	public void affiche(Graphics g) {
		g.setColor(this.couleur);
		g.fillPolygon(p);
		super.affiche(g);
	}
	/**
	 * methode permettant de modifier les points de la figure quand 
	 * on la bouge
	 * @param p, le nouveau tableau de points 
	 */
	public void modifierPoints(Point[] points) {
		this.p = new Polygon();
		for (int i = 0; i < points.length; i++) {
			this.tab_mem[i] = points[i];
			this.p.addPoint(points[i].rendreX(), points[i].rendreY());
		}
	}
	/**
	 * methode permettant de deplacer la figure
	 * @param p, nouveau tableau de points 
	 */
	@Override
	public void translater(Point[] points) {
		this.p = new Polygon();
		for (int i = 0; i < points.length; i++) {
			this.tab_mem[i] = points[i];
			this.p.addPoint(points[i].rendreX(), points[i].rendreY());
		}
	}
	/**
	 * methode qui regarde si le curseur est dans la figure
	 * @param x, coordonnee x de la souris
	 * @param y, coordonnee y de la souris
	 * @return vrai si le curseur est dans la figure, faux si il n'y est pas
	 */
	public boolean estDedans(double x, double y) {
		return this.p.contains(x, y);
	}
}
