package model;

import java.awt.Polygon;
/**
 * Creation de la classe Quadrilatere qui herite de Polygone
 */
public class Quadrilatere extends Polygone{
	/**
	 * Constructeur de Quadrilatere
	 */
	public Quadrilatere() {
		super();
	}
	/**
	 * methode qui retourne le nombre de points d'un Quadrilatere
	 */
	@Override
	public int nbPoints() {
		return 4;
	}
	/**
	 * methode qui retourne le nombre de clics necessaires pour faire un 
	 * Quadrilatere
	 */
	@Override
	public int nbClics() {
		return 4;
	}
	/**
	 * methode qui permet de copier la figure
	 * @return la figure copiee
	 */
	public FigureColoree copieDefigure() {
		Quadrilatere res = new Quadrilatere();
		res.changeCouleur(this.getCouleur());
		for(int i=0; i < this.nbPoints()-1; i++) {
			res.getTab_mem()[i] = this.getTab_mem()[i];
		}
		
		return res;
		
	}
	/**
	 * methode permettant de deplacer la figure
	 * @param p, nouveau tableau de points 
	 */
	@Override
	public void translater(Point[] p) {
		super.translater(p);
		
	}
}
