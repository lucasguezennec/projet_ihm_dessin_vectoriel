package model;

import java.awt.Polygon;
/**
 * Creation de la classe Rectangle qui herite de Quadrilatere
 */
public class Rectangle extends Quadrilatere{
	/**
	 * Constructeur de Rectangle
	 */
	public Rectangle() {
		super();
	}
	/**
	 * methode retournant le nombre de clics necessaires pour faire un rectangle
	 */
	public int nbClics() {
		return 2;	
	}
	/**
	 * methode permettant de modifier les points de la figure quand 
	 * on la bouge
	 * @param p, le nouveau tableau de points 
	 */
	@Override
	public void modifierPoints(Point[] p) {
		this.setP(new Polygon());
		if(this.nbClics()==2) {
			Point[] res = {p[0],new Point(p[1].rendreX(),p[0].rendreY()),p[1],new Point(p[0].rendreX(), p[1].rendreY())};
			for(int i = 0; i < res.length;i++) {
				this.tab_mem[i]=res[i];
				this.getP().addPoint(res[i].rendreX(), res[i].rendreY());
			}

		}
	}
	/**
	 * methode permettant de deplacer la figure
	 * @param p, nouveau tableau de points 
	 */
	public void translater(Point[] points) {
		this.setP(new Polygon());
		for (int i = 0; i < points.length; i++) {
			this.tab_mem[i] = points[i];
			this.getP().addPoint(points[i].rendreX(), points[i].rendreY());
		}
	}
	/**
	 * methode qui permet de copier la figure
	 * @return la figure copiee
	 */
	public FigureColoree copieDefigure() {
		Rectangle res = new Rectangle();
		res.changeCouleur(this.getCouleur());
		for(int i=0; i < this.nbPoints()-1; i++) {
			res.getTab_mem()[i] = this.getTab_mem()[i];
		}
		return res;
	}

}
