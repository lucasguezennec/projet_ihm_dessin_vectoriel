package model;

import java.awt.Polygon;
/**
 * Creation de la classe Triangle qui herite de Polygone
 */
public class Triangle extends Polygone{
	/**
	 * Constructeur de Triangle
	 */
	public Triangle() {
		super();
	}
	/**
	 * methode retournant le nombre de clics necessaire pour faire un Triangle
	 */
	@Override
	public int nbClics() {
		
		return 3;
	}
	/**
	 * methode permettant de modifier les points de la figure quand 
	 * on la bouge
	 * @param p, le nouveau tableau de points 
	 */
	@Override
	public void modifierPoints(Point[] p) {
		this.setP(new Polygon());
		if(this.nbClics()==3) {
			Point[] res = {p[0],p[1],p[2]};
			for(int i = 0; i < res.length;i++) {
				this.tab_mem[i]=res[i];
				this.getP().addPoint(res[i].rendreX(), res[i].rendreY());
			}

		}
	}
	/**
	 * methode permettant de deplacer la figure
	 * @param p, nouveau tableau de points 
	 */
	@Override
	public void translater(Point[] points) {
		this.setP(new Polygon());
		for (int i = 0; i < points.length; i++) {
			this.tab_mem[i] = points[i];
			this.getP().addPoint(points[i].rendreX(), points[i].rendreY());
		}
	}
	/**
	 * methode qui permet de copier la figure
	 * @return la figure copiee
	 */
	@Override
	public FigureColoree copieDefigure() {
		Triangle res = new Triangle();
		res.changeCouleur(this.getCouleur());
		for(int i=0; i < this.nbPoints()-1; i++) {
			res.getTab_mem()[i] = this.getTab_mem()[i];
		}
		return res;
	}
	/**
	 * methode retournant le nombre de points d'un Triangle
	 */
	@Override
	public int nbPoints() {
		return 3;
	}
}
