package projet_ihm_dessin_vectoriel;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;

import controleur.FabricantFigures;
import controleur.PanneauChoix;
import model.DessinModele;
import vue.VueDessin;
/**
 * Creation de la classe Fenetre qui herite de JFrame
 */
public class Fenetre extends JFrame {

	/**
	 * Constructeur de Fenetre
	 * @param s, nom de la fenetre
	 * @param pX, largeur de la fenetre
	 * @param pY, hauteur de la fenetre
	 */
	public Fenetre(String s, int pX, int pY) {
		this.setTitle(s);
		this.setSize(pX, pY);
	}
	/**
	 * methode main qui construit la fenetre ou va se derouler les dessins
	 * @param args, tableau d'arguments
	 */
	public static void main(String[] args) {
		//modele
		DessinModele modele = new DessinModele();
		//Vue
		VueDessin vd = new VueDessin();
		vd.setPreferredSize(new Dimension(800,500));
		//Liaison Modele-Vue
		modele.addObserver(vd);
		//Controleur
		PanneauChoix pc = new PanneauChoix(modele, vd);
		
		
		
		//Fenetre
		Fenetre f = new Fenetre("Dessin Vectoriel",800,800);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setLayout(new BorderLayout());
		f.add(vd,BorderLayout.CENTER);
		f.add(pc, BorderLayout.NORTH);
		f.pack();
		f.setVisible(true);
	}
}
