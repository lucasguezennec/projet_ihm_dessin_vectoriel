package vue;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

import model.DessinModele;
import model.FigureColoree;
import model.FigureMainLevee;
import model.Point;
/**
 * Creation de la classe VueDessin qui implemente Observer
 */
public class VueDessin extends JPanel implements Observer  {
	/**
	 * attribut Liste de FigureColoree qui contiendra la liste des figures
	 */
	private ArrayList<FigureColoree> lfg;
	/**
	 * attribut Liste de FigureMainLevee qui contiendra la liste des traces
	 */
	private ArrayList<FigureMainLevee> listeFigureMainLevee;
	/**
	 * attribut FigureMainLevee qui contiendra le trace en cours
	 */
	private FigureMainLevee figureEnCours;
	/**
	 * attribut DessinModele qui contiendra le dessin voulu
	 */
	private DessinModele dm;
	/**
	 * Constructeur de VueDessin
	 */
	public VueDessin() 
	{
		this.lfg=new ArrayList<FigureColoree>();
		this.listeFigureMainLevee = new ArrayList<FigureMainLevee>();
		this.figureEnCours = new FigureMainLevee();
	}
	/**
	 * methode qui met a jour le composant
	 */
	@Override
	public void update(Observable o, Object arg) {
		this.lfg = ((DessinModele)o).getListeFigure();
		this.listeFigureMainLevee = ((DessinModele)o).getListeMainLevee();
		this.figureEnCours = ((DessinModele)o).getFigureMainLeveeEnCours();
		repaint();
		
	}
	/**
	 * methode qui affiche les figures qu'il y a dans la liste des figures
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		for (int i = 0; i < this.lfg.size(); i++) {
			this.lfg.get(i).affiche(g);
		}
		
		for(int i = 0; i<this.listeFigureMainLevee.size();i++) {
			g.setColor(this.listeFigureMainLevee.get(i).getC());
			for(int j = 0; j<this.listeFigureMainLevee.get(i).getFigureMainLeveeEnCours().size()-1;j++) {
				g.drawLine(this.listeFigureMainLevee.get(i).getFigureMainLeveeEnCours().get(j).rendreX(), 
						this.listeFigureMainLevee.get(i).getFigureMainLeveeEnCours().get(j).rendreY(),
						this.listeFigureMainLevee.get(i).getFigureMainLeveeEnCours().get(j+1).rendreX(), 
						this.listeFigureMainLevee.get(i).getFigureMainLeveeEnCours().get(j+1).rendreY());
				}

		}
	}
}
